// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DacanayMoba/DacanayMobaGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDacanayMobaGameModeBase() {}
// Cross Module References
	DACANAYMOBA_API UClass* Z_Construct_UClass_ADacanayMobaGameModeBase_NoRegister();
	DACANAYMOBA_API UClass* Z_Construct_UClass_ADacanayMobaGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_DacanayMoba();
// End Cross Module References
	void ADacanayMobaGameModeBase::StaticRegisterNativesADacanayMobaGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ADacanayMobaGameModeBase_NoRegister()
	{
		return ADacanayMobaGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ADacanayMobaGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DacanayMoba,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "DacanayMobaGameModeBase.h" },
		{ "ModuleRelativePath", "DacanayMobaGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADacanayMobaGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::ClassParams = {
		&ADacanayMobaGameModeBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADacanayMobaGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADacanayMobaGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADacanayMobaGameModeBase, 1691945715);
	template<> DACANAYMOBA_API UClass* StaticClass<ADacanayMobaGameModeBase>()
	{
		return ADacanayMobaGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADacanayMobaGameModeBase(Z_Construct_UClass_ADacanayMobaGameModeBase, &ADacanayMobaGameModeBase::StaticClass, TEXT("/Script/DacanayMoba"), TEXT("ADacanayMobaGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADacanayMobaGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
